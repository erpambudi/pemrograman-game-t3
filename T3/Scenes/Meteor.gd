extends RigidBody2D

export (int) var GRAVITY =  100
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector2() 

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	velocity.y += delta * GRAVITY  

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
